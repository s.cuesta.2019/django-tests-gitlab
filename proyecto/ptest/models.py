from django.db import models

class Contenido(models.Model):
    name = models.CharField(max_length=200)
    content = models.TextField()

