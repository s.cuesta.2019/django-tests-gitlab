from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from .form import ContentForm
from .models import Contenido

class Counter:
    def __init__(self):
        self.count = 0

    def increment(self):
        self.count += 1
        return self.count

counter = Counter()

def index(request):
    pages = Contenido.objects.all()
    return render(request, 'ptest/index.html', {
        'item_list': pages,
        'count': counter.increment()
    })

@csrf_exempt
def content(request, name):
    p = None
    status = 200
    if request.method == 'POST':
        p, created = Contenido.objects.get_or_create(name=name)
        form = ContentForm(request.POST)
        if form.is_valid():
            p.content = form.cleaned_data['content']
            p.save()

    if request.method == 'GET' or request.method == 'POST':
        if p is None:
            try:
                p = Contenido.objects.get(name=name)
                content_form = ContentForm(initial={'content': p.content})
            except Contenido.DoesNotExist:
                content_form = ContentForm()
                status = 404
        else:
            content_form = ContentForm(initial={'content': p.content})

        content_template = loader.get_template('ptest/content.html')
        content_html = content_template.render({
            'content': name,
            'form': content_form,
            'count': counter.increment()
        }, request)
        return HttpResponse(content_html, status=status)